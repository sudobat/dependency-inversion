<?php

namespace SOLID\Before\DependencyInversion\Models;

class User
{
    private $id;

    private $profiles;

    private $defaultProfile;

    public function __construct(int $id, array $profiles, FacebookProfile $defaultProfile)
    {
        $this->id               = $id;
        $this->profiles         = $profiles;
        $this->defaultProfile   = $defaultProfile;
    }

    public function getDefaultProfile(): FacebookProfile
    {
        return $this->defaultProfile;
    }

    public function addProfile(FacebookProfile $newProfile)
    {
        $newProfile->setUserId($this->id);
        $this->profiles[] = $newProfile;
    }
}
