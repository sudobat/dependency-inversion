<?php

namespace SOLID\Before\DependencyInversion\Models;

class FacebookProfile
{
    private $user;

    private $accessToken;

    private $facebookUserId;

    public function __construct()
    {
    }

    public function requestAccessToken()
    {
        $this->serviceCall('https://api.facebook.com/blablabla');
    }

    public function authenticationCallback(Request $request)
    {
        $this->setAccessToken($request->get('access-token'));
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    private function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function getFacebookUserId(): int
    {
        return $this->facebookUserId;
    }

    public function setFacebookUserId(int $facebookUserId)
    {
        $this->facebookUserId = $facebookUserId;
    }
}
