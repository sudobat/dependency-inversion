<?php

namespace SOLID\Before\DependencyInversion\Repositories;

class MySQLUserRepository
{
    private $database;

    public function __construct(PDo $database)
    {
        $this->database = $database;
    }

    public function find(int $userId): User
    {
        $userData = $this->database->query('SELECT * FROM users WHERE id = ?', [
            'id' => $userId
        ]);

        $user = new User([
            // $userData[...]
        ]);

        return $user;
    }

    public function all(): array
    {
        $usersData = $this->database->query('SELECT * FROM users');

        $users = [];
        foreach ($usersData as $userData) {
            $user = new User([
                // $userData[...]
            ]);

            $users[] = $user;
        }

        return $users;
    }
}
