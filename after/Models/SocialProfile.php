<?php

namespace SOLID\After\DependencyInversion\Models;

interface SocialProfile
{
    private $user;

    private $accessToken;

    private $facebookUserId;

    public function __construct()
    {
    }

    public function requestAccessToken();

    public function authenticationCallback(Request $request);

    public function getUser(): User;

    public function setUser(User $user);

    public function getAccessToken(): string;

    private function setAccessToken(string $accessToken);

    public function getSocialUserId(): int;

    public function setSocialUserId(int $facebookUserId);
}
