<?php

namespace SOLID\After\DependencyInversion\Models;

class User
{
    private $id;

    private $profiles;

    private $defaultProfile;

    public function __construct(int $id, array $profiles, SocialProfile $defaultProfile)
    {
        $this->id               = $id;
        $this->profiles         = $profiles;
        $this->defaultProfile   = $defaultProfile;
    }

    public function getDefaultProfile(): SocialProfile
    {
        return $this->defaultProfile;
    }

    public function setDefaultProfile(SocialProfile $profile)
    {
        
    }

    public function addProfile(SocialProfile $newProfile)
    {
        $newProfile->setUser($this);
        $this->profiles[] = $newProfile;
    }
}
