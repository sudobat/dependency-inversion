<?php

namespace SOLID\After\DependencyInversion\Repositories;

interface UserRepository
{
    public function __construct(MySQLDatabase $database);

    public function find(int $userId): User;

    public function all(): array;
}
