<?php

namespace SOLID\After\DependencyInversion\Controllers;

class UserController extends Controller
{
    public function show(UserRepository $userRepository, int $userId)
    {
        $user = $userRepository->find($userId);

        if (! $user) {
            return abort(404);
        }

        return view('user.single')->with([
            'fullname'          => $post->getFullName(),
            'description'       => $post->getDescription(),
            'createdAt'         => $post->getCreatedAt()
        ]);
    }

    public function all(UserRepository $userRepository)
    {
        $users = $userRepository->all();

        return view('user.list')->with([
            'users' => $users
        ]);
    }
}
